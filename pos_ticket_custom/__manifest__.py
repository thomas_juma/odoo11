# -*- coding: utf-8 -*-
##############################################################################
#       
#    Decor dura receipt template
##############################################################################
{
    'name': 'Custom receipt template',
    'summary': """Custom POS Ticket""",
    'version': '11.0.1.0',
    'description': """Change the size and design of the standard odoo receipt template.""",
    'author': 'Thomas',
    'category': 'Point of Sale',
    'depends': ['base', 'point_of_sale'],
    'data': ['views/pos_ticket_assets.xml'],
    'qweb': ['static/src/xml/pos_ticket_text.xml'],
    # 'images': ['static/description/decordura.jpg'],
    'demo': [],
    'installable': True,
    'auto_install': False,

}
