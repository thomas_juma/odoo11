# -*- coding: utf-8 -*-
{
    'name': "dotmatrixprinter",

    'summary': """
        This is a module used for printing the invoice and pos reciept directly on dot
        matrix printer""",

    'description': """
        # =======================================================================
        - print pos reciept, invoice directly on dot-matrix printer.
        - no special hardware needed.
        - using printer proxy script (apache/nginx+php)
        - add printer_data field on account.invoice, receipt
        - printer template data from mail.template named "Dot Matrix *"
        
        Installation
        # ==============================================================================
        -Install this addon on the database
        -Download the print.php script "vit_dotmatrix/static/print.php"
        -Install apache+php or nginx+php on the local computer and copy print.php script to the htdocs
        -Do your printing on the dotmatrix printer
    """,

    'author': "Thomas Juma - Kylix-Holdings",
    'website': "http://www.kylix-holdings.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['sale', 'account', 'stock', 'purchase', 'mail', 'point_of_sale'],

    'external_dependencies': {
        'python': [
        ]
    },

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/web_asset.xml',
        'views/pos_assets.xml',
        'views/invoice.xml',
        'data/templates.xml',
    ],
    'qweb':[
        'static/src/xml/web_print_button.xml',
        'static/src/xml/pos_print_button.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],

    "installable": True,
    "auto_install": False,
    "application": True,
}