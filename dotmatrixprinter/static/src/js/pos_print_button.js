odoo.define('dotmatrixprinter.custom_button', function (require) {
    
    "use strict";
    var core = require('web.core');
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');





    //Custom Code
    // var CustomButton = screens.ActionButtonWidget.extend({
    //     template: 'CustomButton',

    //     button_click: function(){

    //     var self = this;
    //     self.custom_function();

    //     },

    //     custom_function: function(){
    //         console.log('Hi I am button click of CustomButton');
    //     }

    // });

    // screens.define_action_button({
    //     'name': 'custom_button',
    //     'widget': CustomButton,
    // });


//     // receipt screen

    var CustomReceiptScreen = gui.PosTicket.extend({
        //prepend template using jquery
        template: 'CustomReceiptTemplate',

        show: function() {
            this._super();
            var self = this;

            this.render_receipt();
            this.print();
        },

        print: function() {
            console.log("print receipt details.");
        },

        render_receipt: function() {
            this.$('.pos-custom-receipt-container').html(QWeb.render('PosTicket', this.get_receipt_render_env()));
        },


    });

    gui.define_screen({name:'custom_receipt', widget: CustomReceiptScreen});


});