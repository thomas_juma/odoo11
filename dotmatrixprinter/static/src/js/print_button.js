// // web print data custom js
// odoo.define('dotmatrixprinter.print_button', function (require) {
//     "use strict";

//     var core =require('web.core');
//     var FormRenderer = require('web.FormRenderer');
//     var FormView = require('web.FormView')
//     var mixins = require('web.mixins');
//     var Widget = require('web.Widget');

//     var QWeb = core.qweb;
//     var _t = core._t;

//     var print_button = FormRenderer.include({

//         events: _.extend({}, print_button.prototype.events, {
//             'click': '_onClick',
//         }),

//         _onClick: function (e) {
//             if (this.node.attrs.name === 'print') {
//                 e.preventDefault();

//                 console.log("printing solution");

//             }
//         },

    

        
//     });

//     return print_button;

// });




















// web print data custom js
odoo.define('dotmatrixprinter.print_button', function (require) {
    "use strict";

    var core =require('web.core');
    var FormRenderer = require('web.FormRenderer');
    var FormView = require('web.FormView')
    var mixins = require('web.mixins');
    var Widget = require('web.Widget');

    var QWeb = core.qweb;
    var _t = core._t;

    var print_button = FormRenderer.extend({

        // events: _.extend({}, print_button.prototype.events, {
        //     'button_click': '_addOnClickAction',
        // }),


        _addOnClickAction: function ($el, node) {
            var self = this;
            self.$el.find(("[name='print']")).click(function () {


                console.log("print triggered");
                var url = "http://localhost/var/www/html/dotmatrix/";

                if(node.attrs.name === "print"){
                    url = url + "print.php";
                }

                var view = self.getParent();
                var printer_data = view.printer_data;
                if(!printer_data){
                    alert('No data to print. Please click "Update Printer Data"');
                    return;
                }


                // code ajax printer
                console.log(printer_data);
                // urlencode() // jsonp() //json.stringify
                $.ajax({
                    method: "POST",
                    // datatype: "JSON",
                    // crossDomain: true,
                    url: url,
                    data: {
                        printer_data: printer_data
                   },
                   //jsonpCallback: "postHandle",
                   //contentType: "application/jsonp",
                   success: function(data){
                        console.log('Success');
                        console.log('data');
                    },
                   error: function(data){
                         console.log('Failed');
                         console.log('data');
                    },
                });
           



                self.trigger_up('_addOnClickAction', {
                    attrs: node.attrs.name,
                    record: self.state,
                });

            });
        
        },
   

        
    });

    return print_button;

});