// web print data custom js
odoo.define('dotmatrixprinter.print_button', function (require) {
    "use strict";

    var core =require('web.core');
    var FormRenderer = require('web.FormRenderer');
    var mixins = require('web.mixins');
    var Widget = require('web.Widget');

    var QWeb = core.qweb;
    var _t = core._t;

    var print_button = FormRenderer.extend({


        _addOnClickAction: function ($el, node) {
            var self = this;
            $el.click(function () {
                if(this.node.attrs.custom === "print"){
                console.log("print triggered");
                var url = "http://localhost/var/www/html/dotmatrix/";

                if(this.node.attrs.custom === "print"){
                    url = url + "print.php";
                }

                // var view = this.getParent();
                // var printer_data = view.datarecord.printer_data;
                // if(!printer_data){
                //     alert('No data to print. Please click "Update Printer Data"');
                //     return;
                // }


                // code ajax printer
                console.log(printer_data);
                // urlencode() // jsonp() //json.stringify
                $.ajax({
                    method: "POST",
                    // datatype: "JSON",
                    // crossDomain: true,
                    url: url,
                    data: {
                        printer_data: printer_data
                   },
                   //jsonpCallback: "postHandle",
                   //contentType: "application/jsonp",
                   success: function(data){
                        console.log('Success');
                        console.log('data');
                    },
                   error: function(data){
                         console.log('Failed');
                         console.log('data');
                    },
                });
            }
                else{
                    this._super();
                }


                self.trigger_up('print_button', {
                    attrs: node.attrs,
                    record: self.state,
                });
            });
        },


        
    });

    return print_button;

});